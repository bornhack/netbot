from errbot import BotPlugin, botcmd
import subprocess


class Netinfo(BotPlugin):

    @botcmd  # flags a command
    def routes(self, msg, args):  # a command callable with !tryme
        """
        show routes on this device
        """
        yield "Showing my routing table"
        for l in self.get_routes():
            yield l
        yield "(done)"

    def get_routes( self ):
        process = subprocess.Popen(['/bin/ip', 'route'],
                     stdout=subprocess.PIPE,
                     stderr=subprocess.PIPE)


        stdout, stderr = process.communicate()

        print(stdout)
        return stdout.decode('utf-8').split('\n')

    @botcmd
    def ssids(self, msg, args ):
        """
        Do a wifi scan and return the result
        """
        yield "Doing an wifi scan"
        for l in self.scan_wifi():
            yield l
        yield "(done)"

    def scan_wifi(self):
        process = subprocess.Popen(['sudo', '../../scripts/show_ssids.py'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)


        stdout, stderr = process.communicate()

        print(stdout)
        return stdout.decode('utf-8').split('\n')