import logging

# This is a minimal configuration to get you started with the Text mode.
# If you want to connect Errbot to chat services, checkout
# the options in the more complete config-template.py from here:
# https://raw.githubusercontent.com/errbotio/errbot/master/errbot/config-template.py


##########################################################################
# Core Errbot configuration                                              #
##########################################################################

#BACKEND = 'Text'  # Errbot will start in text mode (console only mode) and will answer commands from there.
BACKEND = 'IRC'

BOT_DATA_DIR = r'./data'
BOT_EXTRA_PLUGIN_DIR = r'./plugins'

BOT_LOG_FILE = r'./errbot.log'
BOT_LOG_LEVEL = logging.DEBUG

# If you want only a subset of the core plugins that are bundled with errbot, you can specify them here.
# CORE_PLUGINS = None # This is default, all core plugins.
# For example CORE_PLUGINS = ('ACLs', 'Backup', 'Help') you get those names from the .plug files Name entry.
# For absolutely no plug: CORE_PLUGINS = ()
#CORE_PLUGINS = ('IRC', 'ACL', 'Backup', 'help', 'Examples', )


##########################################################################
# Account and chatroom (MUC) configuration                               #
##########################################################################

BOT_ADMINS = ('@moozer', )  # !! Don't leave that to "@CHANGE_ME" if you connect your errbot to a chat system !!

BOT_IDENTITY = {
    'nickname' : 'moz-nocbot-test',
    # 'username' : 'err-chatbot',    # optional, defaults to nickname if omitted
    # 'password' : None,             # optional
    'server' : 'irc.baconsvin.org',
    'port': 6697,                  # optional
    'ssl': True,                  # optional
    # 'ipv6': False,                 # optional
    # 'nickserv_password': None,     # optional
    
    ## Optional: Specify an IP address or hostname (vhost), and a
    ## port, to use when making the connection. Leave port at 0
    ## if you have no source port preference.
    ##    example: 'bind_address': ('my-errbot.io', 0)
    # 'bind_address': ('localhost', 0),
}

CHATROOM_PRESENCE = ('#bornhack-noc-test',)

ACCESS_CONTROLS = { 'ChatRoom:*': {'allowusers': BOT_ADMINS},
                    'Flows:*': {'allowusers': BOT_ADMINS},
                    'Health:*': {'allowusers': BOT_ADMINS},
                    'Plugins:*': {'allowusers': BOT_ADMINS},
                    'Utils:*': {'allowusers': BOT_ADMINS},
                  }

#ACCESS_CONTROLS = {'status': {'allowrooms': ('someroom@conference.localhost',)},
#                   'about': {'denyusers': ('*@evilhost',), 'allowrooms': ('room1@conference.localhost', 'room2@conference.localhost')},
#                   'uptime': {'allowusers': BOT_ADMINS},
#                   'help': {'allowmuc': False},
#                   'help': {'allowmuc': False},
#                    'ChatRoom:*': {'allowusers': BOT_ADMINS},
#                  }

##########################################################################
# Prefix configuration                                                   #
##########################################################################
BOT_PREFIX = 'moz-nocbot-test, '
BOT_ALT_PREFIXES = ('moz-nocbot-test', )
BOT_ALT_PREFIX_SEPARATORS = (':', ',', ';')
BOT_PREFIX_OPTIONAL_ON_CHAT = True

##########################################################################
# Access controls and message diversion                                  #
##########################################################################

HIDE_RESTRICTED_COMMANDS = True


##########################################################################
# Miscellaneous configuration options                                    #
##########################################################################
